---
title: "EDISON 2018: NONMEM estimation using R"
author: "Sungpil Han"
date: "`r Sys.Date()`"
knit: "bookdown::render_book"
documentclass: krantz
bibliography: ["bib/ref.bib", "bib/packages.bib"]
biblio-style: apalike
link-citations: yes
colorlinks: yes
lot: yes
lof: yes
fontsize: 12pt
monofont: "Source Code Pro"
monofontoptions: "Scale=0.7"
site: bookdown::bookdown_site
description: ""
url: 'https\://book-nmw.netlify.com/'
github-repo: asancpt/book-nmw
cover-image: assets/cover.jpg
output:
  bookdown::gitbook:
    df_print: kable
    dev: svglite
    css: style.css
    split_by: chapter
    config:
      toc:
        collapse: chapter
        before: |
          <li><a href="./index.html">NONMEM estimation using R</a></li>
        after: |
          <li><a href="https://bookdown.org" target="blank">Published with bookdown</a></li>
      #download: [pdf, epub, mobi]
      edit: https://github.com/rstudio/bookdown/edit/master/inst/examples/%s
      sharing:
        github: yes
        facebook: no
  bookdown::html_chapters:
    css: [css/style.css, css/toc.css]
  bookdown::pdf_book:
    includes:
      in_header: latex/preamble.tex
      before_body: latex/before_body.tex
      after_body: latex/after_body.tex
    keep_tex: yes
    dev: "cairo_pdf"
    latex_engine: xelatex
    citation_package: natbib
    template: null
    pandoc_args: --top-level-division=chapter
    toc_depth: 3
    toc_unnumbered: no
    toc_appendix: yes
    quote_footer: ["\\VA{", "}{}"]
  bookdown::epub_book:
    dev: svglite
    stylesheet: css/style.css
editor_options: 
  chunk_output_type: console
---

```{r setup, include = FALSE}
library(knitr)
opts_chunk$set(message = FALSE, warning = FALSE, error = TRUE)
```
# Abstract

TBD

# Introduction

TBD

# Materials and Methods

TBD

## nmw R package

NONMEM Workshop 2017에서 사용된 nmw 패키지입니다. [@Kim_2015;@Bae_2016;@R-nmw]

먼저 설치를 해야 합니다. 

```{r}
#install.packages(c('nmw', 'compiler')) # CRAN
library(nmw)
# library(compiler)
# enableJIT(3)
```

`THEO-FO.OUT`와 비교할 때 값이 같습니다.

Scaling factor는 소스 코드를 보고 알아낸 것입니다.

## Datasets

<!--```{r, code = readLines('lib/nmw/inst/doc/nmw-vignette.R'), cache = TRUE}
```-->

```{r}
DataAll = Theoph
colnames(DataAll) = c("ID", "BWT", "DOSE", "TIME", "DV")
DataAll[,"ID"] = as.numeric(as.character(DataAll[,"ID"]))
```

그림을 그려보면 다음과 같습니다. (Figure \@ref(fig:xyplot))

```{r xyplot, fig.cap = 'xyplot'}
require(lattice)
xyplot(DV ~ TIME | as.factor(ID), data=DataAll, type="b")
```

# Results 

$\theta, \eta, \epsilon$ 개수를 먼저 지정해 주고 초기값을 설정합니다.

```{r}
DataAll = Theoph
colnames(DataAll) = c("ID", "BWT", "DOSE", "TIME", "DV")
DataAll[,"ID"] = as.numeric(as.character(DataAll[,"ID"]))

nTheta = 3
nEta = 3
nEps = 2

THETAinit = c(2, 50, 0.1)
OMinit = matrix(c(0.2, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1, 0.2), nrow=nEta, ncol=nEta)
SGinit = diag(c(0.1, 0.1))

LB = rep(0, nTheta) # Lower bound
UB = rep(1000000, nTheta) # Upper bound

FGD = deriv(~DOSE/(TH2*exp(ETA2))*TH1*exp(ETA1)/(TH1*exp(ETA1) - TH3*exp(ETA3))*
              (exp(-TH3*exp(ETA3)*TIME)-exp(-TH1*exp(ETA1)*TIME)),
            c("ETA1","ETA2","ETA3"),
            function.arg=c("TH1", "TH2", "TH3", "ETA1", "ETA2", "ETA3", "DOSE", "TIME"),
            func=TRUE,
            hessian=(e$METHOD == "LAPL"))
H = deriv(~F + F*EPS1 + EPS2, c("EPS1", "EPS2"), function.arg=c("F", "EPS1", "EPS2"), func=TRUE)

PRED = function(THETA, ETA, DATAi)
{
  FGDres = FGD(THETA[1], THETA[2], THETA[3], ETA[1], ETA[2], ETA[3], DOSE=320, DATAi[,"TIME"]) 
  Gres = attr(FGDres, "gradient")
  Hres = attr(H(FGDres, 0, 0), "gradient")
  
  if (e$METHOD == "LAPL") {
    Dres = attr(FGDres, "hessian")
    Res = cbind(FGDres, Gres, Hres, Dres[,1,1], Dres[,2,1], Dres[,2,2], Dres[,3,])
    colnames(Res) = c("F", "G1", "G2", "G3", "H1", "H2", "D11", "D21", "D22", "D31", "D32", "D33") 
  } else {
    Res = cbind(FGDres, Gres, Hres)
    colnames(Res) = c("F", "G1", "G2", "G3", "H1", "H2") 
  }
  return(Res)
}



```



####### First Order Approximation Method
InitStep(DataAll, THETAinit=THETAinit, OMinit=OMinit, SGinit=SGinit, LB=LB, UB=UB, 
         Pred=PRED, METHOD="ZERO")
(EstRes = EstStep())           # 4 sec
(CovRes = CovStep())           # 2 sec
PostHocEta() # Using e$FinalPara from EstStep()
TabStep()

## First Order Approximation Method

```{r}
InitStep(DataAll, THETAinit=THETAinit, OMinit=OMinit, SGinit=SGinit, LB=LB, UB=UB, 
         Pred=PRED, METHOD="ZERO")
```

먼저 `InitStep()`을 통해서 초기화를 합니다.

### EstRes

```{r}
(EstRes = EstStep())           # 4 sec
```

### CovRes

```{r}
(CovRes = CovStep())           # 2 sec
```

### PostHocEta

```{r}
PostHocEta() # FinalPara from EstStep()
```

### TabStep

```{r}
TabStep()
```


## First Order Conditional Estimation with Interaction Method

```{r eval = FALSE}
InitStep(DataAll, THETAinit=THETAinit, OMinit=OMinit, SGinit=SGinit, nTheta=nTheta, LB=LB, UB=UB, Pred=PRED, METHOD="COND")
(EstRes = EstStep())           # 1.7 min
(CovRes = CovStep())           # 44 sec
get("EBE", envir=e)
```

## Laplacian Approximation with Interacton Method

```{r eval = FALSE}
InitStep(DataAll, THETAinit=THETAinit, OMinit=OMinit, SGinit=SGinit, nTheta=nTheta, LB=LB, UB=UB, Pred=PRED, METHOD="LAPL")
(EstRes = EstStep())           # 3.4 min
(CovRes = CovStep())           # 52 sec
get("EBE", envir=e)
```


$$
G =
  \begin{bmatrix}
    \frac{\partial F}{\partial \eta_1} & \frac{\partial^2 F}{\partial \eta_1 \partial \eta_1}  & [blank] & [blank] \\
    \frac{F}{\eta_2} & \frac{\partial^2 F}{\partial \eta_2 \partial \eta_1} & \frac{\partial^2 F}{\partial \eta_2 \partial \eta_2} & [blank] \\
    \frac{F}{\eta_3} & \frac{\partial^2 F}{\partial \eta_3 \partial \eta_1} & \frac{\partial^2 F}{\partial \eta_3 \partial \eta_2} & \frac{\partial^2 F}{\partial \eta_3 \partial \eta_3}
  \end{bmatrix}
$$




```{r, echo = FALSE, eval = FALSE}
DiagrammeR::mermaid("
graph LR
A --> B
", height = 200)
```

# Discussion

# Acknowledgement

# References {-}

```{r, include = FALSE}
knitr::write_bib(file = 'bib/packages.bib')
```

